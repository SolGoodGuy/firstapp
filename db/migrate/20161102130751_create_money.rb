class CreateMoney < ActiveRecord::Migration[5.0]
  def change
    create_table :money do |t|
      t.float :Total
      t.float :Debit
      t.float :Credit

      t.timestamps
    end
  end
end
