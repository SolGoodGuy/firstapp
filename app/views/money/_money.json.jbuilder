json.extract! money, :id, :Total, :Debit, :Credit, :created_at, :updated_at
json.url money_url(money, format: :json)